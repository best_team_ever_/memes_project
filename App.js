import React from 'react';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './components/HomeScreen'
import LoadingScreen from "./components/LoadingScreen";
import {applyMiddleware, createStore} from 'redux';
import rootReducer from './reducers/index';
import { Provider } from 'react-redux';
import {createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers';
import CategoryScreen from "./components/CategoryScreen";
import Constructor from "./components/Constructor";


const RootStack = StackNavigator(
  {
    Loading: {
      screen: LoadingScreen,
      path: 'loading',
    },
    Home: {
      screen: HomeScreen,
      path: 'home',
    },
    Category: {
      screen: CategoryScreen,
      path: 'category'
    },
    Constructor: {
      screen: Constructor,
      path: 'constructor'
    }
  },
  {
    initialRouteName: 'Loading',
  }
);


const navReducer = (state, action) => {
  const newState = RootStack.router.getStateForAction(action, state);
  return newState || state;
};


const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);


export default class App extends React.Component {
  render() {
    return (
      <Provider store={createStore(rootReducer(navReducer), applyMiddleware(middleware))}>
        <RootStack/>
      </Provider>
    );
  }
}