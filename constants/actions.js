export const GET_CATEGORIES = 'GET_CATEGORIES';
export const LOAD_DONE = 'LOAD_DONE';
export const GET_POPULAR_CHARACTERS = 'GET_POPULAR_CHARACTERS';
export const GET_SEARCH_CHARACTERS = 'GET_SEARCH_CHARACTERS';
export const LAST_GET_SEARCH_CHARACTERS_TEXT = 'LAST_GET_SEARCH_CHARACTERS_TEXT';
export const GET_SUBCATEGORIES = 'GET_SUBCATEGORIES';
export const GET_CATEGORY_MEMES = 'GET_CATEGORY_MEMES';
export const GET_MEME = 'GET_MEME';