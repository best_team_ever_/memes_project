import React from "react";
import { Image } from "react-native";

// Home images
export const SearchIcon = () => <Image source={require('../static/images/icons/common/search_icon_magnifier.png')} style={{width: 30, height: 30}}/>;
export const LoadingImage = ({width, height}) => <Image source={require('../static/images/loading.png')} style={{width: width, height: height}}/>;

// Constructor images
export const CloseBtn = ({width, height}) => <Image source={require('../static/images/icons/constructor/constructor_icon_close.png')} style={{height: 16, width: 16}}/>;
export const FontBtn = ({width, height}) => <Image source={require('../static/images/icons/constructor/constructor_icon_addText.png')} style={{height: 16, width: 24}}/>;
export const BackgroundBtn = ({width, height}) => <Image source={require('../static/images/icons/constructor/constructor_icon_addBg.png')} style={{height: 16 , width: 16}}/>;
export const NextBtn = ({width, height}) => <Image source={require('../static/images/icons/constructor/constructor_icon_next.png')} style={{height: 16, width: 16}}/>;

// GroupTypes images
export const EyesGroup = ({width, height, flex}, active) => (
  active?
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_eyes-active.png')} style={{height: 40, width: 40, flex}}/>:
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_eyes.png')} style={{height: 40, width: 40, flex}}/>
);

export const FaceGroup = ({width, height, flex}, active) => (
  active?
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_face-active.png')} style={{height: 40, width: 40, flex}}/>:
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_face.png')} style={{height: 40, width: 40, flex}}/>
);

export const HatGroup = ({width, height, flex}, active) => (
  active?
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_hat-active.png')} style={{height: 40, width: 40, flex}}/>:
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_hat.png')} style={{height: 40, width: 40, flex}}/>
);

export const LeftHandGroup = ({width, height, flex}, active) => (
  active?
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_leftHand-active.png')} style={{height: 40, width: 40, flex}}/>:
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_leftHand.png')} style={{height: 40, width: 40, flex}}/>
);

export const RightHandGroup = ({width, height, flex}, active) => (
  active?
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_rightHand-active.png')} style={{height: 40, width: 40, flex}}/>:
    <Image source={require('../static/images/icons/constructor/constructor_bodyPartIcon_rightHand.png')} style={{height: 40, width: 40, flex}}/>
);
