import React, {Component} from 'react';
// import {Image} from 'react-native';
import {Image as Img} from "react-native-expo-image-cache";


export class CategoryImage extends Component{
  render(){
    const {style, source} = this.props;

    return (
      <Img style={style}
           preview={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=='}
           uri={source.uri}/>
    )
  }
}


export class CharacterImage extends Component{
  render(){
    const {style, source} = this.props;

    return (
      <Img style={style}
           preview={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=='}
           uri={source.uri}/>
    )
  }
}


export class PartGroupImage extends Component{
  render(){
    const {style, source} = this.props;

    return (
      <Img style={style}
           preview={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=='}
           uri={source.uri}/>
    )
  }
}


export class PartGroupFullImage extends Component{
  render(){
    const {style, source} = this.props;

    return (
      <Img style={style} {...{
        uri: source.uri,
        preview: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=='
      }}/>
    )
  }
}