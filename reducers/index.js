import { combineReducers } from 'redux'
import api from './api'

export default rootReducer = (nav) => combineReducers({
  nav,
  api
});
