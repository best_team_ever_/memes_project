import {
  GET_CATEGORIES, LOAD_DONE, GET_POPULAR_CHARACTERS, GET_SEARCH_CHARACTERS, LAST_GET_SEARCH_CHARACTERS_TEXT,
  GET_SUBCATEGORIES, GET_CATEGORY_MEMES, GET_MEME
} from '../constants/actions';


const initialState = {
  loading: true,
  categorises: [],
  popularCharacters: [],
  searchCharacters: [],
  name: '',
  subcategories: {},
  categoryMemes: {},
  memes: {}
};


export default function(state = initialState, action) {
  switch (action.type) {
    case LOAD_DONE:
      return {
        ...state,
        loading: false
      };
    case GET_CATEGORIES:
      return {
        ...state,
        categorises: action.data,
      };
    case GET_POPULAR_CHARACTERS:
      return {
        ...state,
        popularCharacters: action.data
      };
    case GET_SEARCH_CHARACTERS:
      if (action.name === state.name){
        return {
          ...state,
          searchCharacters: action.data
        };
      }else{
        return state;
      }
    case LAST_GET_SEARCH_CHARACTERS_TEXT:
      return {
        ...state,
        name: action.data
      };
    case GET_SUBCATEGORIES:
      return {
        ...state,
        subcategories: {
          ...state.subcategories,
          [action.categoryId]: action.data
        }
      };
    case GET_CATEGORY_MEMES:
      return {
        ...state,
        categoryMemes: {
          ...state.categoryMemes,
          [action.categoryId]: action.data
        }
      };
    case GET_MEME:
      return {
        ...state,
        memes: {
          ...state.memes,
          [action.memeId]: action.data
        }
      };
    default:
      return state;
  }
}
