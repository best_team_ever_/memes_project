import React from 'react';
import { Dimensions } from 'react-native';

// Home ip
// export const CORE_URL = 'http://192.168.1.16:8000/api/v1/';
// export const IMAGE_URL = 'http://192.168.1.16:8000/api/v1/image/';

// qa ip
export const CORE_URL = 'https://qa.lolpixapp.com/api/v1/';
export const IMAGE_URL = 'https://qa.lolpixapp.com/api/v1/image/';


export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;
