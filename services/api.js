import DataProvider from "../utils/provider";
import {CORE_URL} from '../settings';
import {GET_CATEGORIES, LOAD_DONE, GET_POPULAR_CHARACTERS, GET_SEARCH_CHARACTERS, LAST_GET_SEARCH_CHARACTERS_TEXT,
  GET_SUBCATEGORIES, GET_CATEGORY_MEMES, GET_MEME} from "../constants/actions";


export default class ApiProvider extends DataProvider {
  doneLoading(){
    this.dispatch(LOAD_DONE, {})
  }

  getCategories() {
    return fetch(`${CORE_URL}categories/`)
      .then((response) => response.json())
      .then((json)=>this.dispatch(GET_CATEGORIES, {data: json}))
      .catch((error) => {
        console.warn(error);
      });
  }

  getPopularCharacters() {
    return fetch(`${CORE_URL}characters/`)
      .then((response) => response.json())
      .then((json)=>this.dispatch(GET_POPULAR_CHARACTERS, {data: json.results}))
      .catch((error) => {
        console.warn(error);
      });
  }

  searchForCharacter(name){
    this.dispatch(LAST_GET_SEARCH_CHARACTERS_TEXT, {data: name});
    return fetch(`${CORE_URL}characters/?name=${name}`)
      .then((response) => response.json())
      .then((json)=>this.dispatch(GET_SEARCH_CHARACTERS, {data: json.results, name: name}))
      .catch((error) => {
        console.warn(error);
      });
  }

  getSubcategories(categoryId){
    return fetch(`${CORE_URL}categories/${categoryId}/subcategories/`)
      .then((response) => response.json())
      .then((json)=>this.dispatch(GET_SUBCATEGORIES, {data: json.results, categoryId: categoryId}))
      .catch((error) => {
        console.warn(error);
      });
  }

  getCategoryMemes(categoryId){
    return fetch(`${CORE_URL}categories/${categoryId}/memes/`)
      .then((response) => response.json())
      .then((json)=>this.dispatch(GET_CATEGORY_MEMES, {data: json.results, categoryId: categoryId}))
      .catch((error) => {
        console.warn(error);
      });
  }

  getMeme(memeId){
    return fetch(`${CORE_URL}memes/${memeId}/parts/`)
      .then((response) => response.json())
      .then((json)=>this.dispatch(GET_MEME, {data: json.results, memeId: memeId}))
      .catch((error) => {
        console.warn(error);
      });
  }
}
