import React from "react";
import {StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {connect} from "react-redux";
import ApiProvider from "../services/api";
import {IMAGE_URL, SCREEN_HEIGHT} from "../settings";
import {NavigationActions} from "react-navigation";
import {CategoryImage, CharacterImage} from "../utils/image-containers";


const styles = StyleSheet.create({
  subcategory: {
    margin: 8,
    height: 140,
    position: 'relative',
    width: 140,
    overflow: 'hidden'
  },
  subcategoryFake: {
    margin: 8,
    height: 140,
    position: 'relative',
    width: 140,
    overflow: 'hidden',
    backgroundColor: 'transparent'
  },
  imageText: {
    color: '#ffffff',
    fontSize: 24,
    position: 'absolute',
    left: 12,
    top: 5,
    fontWeight: 'bold'
  },
  subcategoryContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  detailContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    // top: 500,
    // height: 300,
    width: '100%',
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: '#D5DBE0'
  },
  opacityContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(122, 122, 122, 0.4)'
  },
  popularCharactersView: {
    backgroundColor: '#ffffff',
    padding: 3,
    borderRadius: 4
  },
  charactersView: {
    padding: 8
  },
  popularCharactersText: {
    color: '#6C7680',
    fontSize: 22
  },
  charactersContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    flexDirection: 'row',
    flexGrow: 1
  }
});


class CategoryScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    const { category } = navigation.state.params;

    return {
      title: category.name,
      headerStyle: {
        backgroundColor: '#6239ab',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  };

  constructor(props){
    super(props);

    this.redirectToConstructor = this.redirectToConstructor.bind(this);
    this.renderSubcategories = this.renderSubcategories.bind(this);
    this.chooseSubcategory = this.chooseSubcategory.bind(this);
    this.renderCharacters = this.renderCharacters.bind(this);
    this.renderCharacter = this.renderCharacter.bind(this);
    this.back = this.back.bind(this);

    this.categoryId = props.navigation.state.params.category.id;
    props.apiProvider.getSubcategories(this.categoryId);
    props.apiProvider.getCategoryMemes(this.categoryId);

    this.state = {
      chosenCategory: 0
    };
  }

  renderSubcategory(subcategory, chosenCategory){
    if (typeof subcategory === 'undefined'){
      return (
        <TouchableOpacity key={0} onPress={()=>{
          this.chooseSubcategory(0);
        }} disabled={true}>
          <View style={styles.subcategoryFake}>
          </View>
        </TouchableOpacity>
      )
    }

    return (
      <TouchableOpacity key={subcategory.id} onPress={()=>{
          this.chooseSubcategory(subcategory.id);
        }} disabled={Boolean(chosenCategory)}>
        <View style={styles.subcategory}>
          <CategoryImage source={{uri:`${IMAGE_URL}${subcategory.image}/`}}
                         style={{height: 140, borderRadius: 4}}/>
          <Text style={styles.imageText}>{subcategory.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  back(){
    this.setState({ chosenCategory: 0 });
  }

  chooseSubcategory(categoryId){
    this.setState({ chosenCategory: categoryId });
    this.props.apiProvider.getCategoryMemes(categoryId);
  }

  renderCharacter(character){
    return (
      <TouchableOpacity key={character.id} onPress={()=>this.redirectToConstructor(character)} style={{width: 100, margin: 8}}>
        <View style={styles.popularCharactersView} key={character.id}>
          <CharacterImage source={{uri:`${IMAGE_URL}${character.image}/`}}
                          style={{width: 90, height: 90}}/>
          <Text style={{fontSize: 16, color: '#6C7680', textAlign: 'center'}}>{character.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  redirectToConstructor(meme){
    this.props.navigation.dispatch(NavigationActions.navigate({ routeName: 'Constructor', params: {meme: meme}}));
  }

  renderSubcategories(){
    const {subcategories} = this.props;
    const {chosenCategory} = this.state;

    let subcategoriesList = subcategories[this.categoryId] || [];

    let categoriesToRender = [];

    for (let i=0; i<Math.ceil(subcategoriesList.length/2); i++){
      categoriesToRender.push(
        <View style={styles.subcategoryContainer} key={i}>
          {this.renderSubcategory(subcategoriesList[i*2], chosenCategory)}
          {this.renderSubcategory(subcategoriesList[i*2+1], chosenCategory)}
        </View>
      )
    }

    return categoriesToRender
  }

  renderCharacters(){
    const {categoryMemes} = this.props;

    let currentCategoryMemes = categoryMemes[this.categoryId] || [];

    return (
      <View style={styles.charactersContainer}>
        {
          currentCategoryMemes.map(character=>this.renderCharacter(character))
        }
      </View>
    )
  }

  render(){
    const {subcategories, categoryMemes} = this.props;
    const {chosenCategory} = this.state;

    let subcategoriesList = subcategories[this.categoryId] || [];

    let categoriesView = (
      <ScrollView key={0} style={{position: 'relative', display: 'flex', flexGrow: 1}}>
        <TouchableOpacity activeOpacity={1} onPress={this.back}
                          style={{position: 'relative', backgroundColor: '#dfdfdf', minHeight: chosenCategory?(SCREEN_HEIGHT-265):(SCREEN_HEIGHT-70)}}>
          {
            subcategoriesList.length?this.renderSubcategories():this.renderCharacters()
          }
          {
            chosenCategory?
              <View key={1} style={styles.opacityContainer}>
              </View>:null
          }
        </TouchableOpacity>
      </ScrollView>
    );

    if (chosenCategory){
      let currentCategoryMemes = categoryMemes[chosenCategory] || [];

      let charactersView = (
        <View style={styles.charactersView} key={1}>
          <Text style={styles.popularCharactersText}>Popular Characters:</Text>
          <ScrollView horizontal={true} style={styles.popularCharactersViewContainer}>
            {
              currentCategoryMemes.map(character=>this.renderCharacter(character))
            }
          </ScrollView>
        </View>
      );

      return [categoriesView, charactersView]
    }else{
      return categoriesView
    }
  }
}

export default connect(
  state => ({
    popularCharacters: state.api.popularCharacters,
    searchCharacters: state.api.searchCharacters,
    categorises: state.api.categorises,
    subcategories: state.api.subcategories,
    categoryMemes: state.api.categoryMemes
  }),
  dispatch => ({
    apiProvider: new ApiProvider(dispatch),
  })
)(CategoryScreen)
