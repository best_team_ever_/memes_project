import React from "react";
import {StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {CategoryImage, CharacterImage} from "../utils/image-containers";
import TopBar from "./TopBar";
import {SearchIcon} from "../constants/images";
import {connect} from "react-redux";
import ApiProvider from "../services/api";
import {IMAGE_URL} from "../settings";
import {NavigationActions} from "react-navigation";


const styles = StyleSheet.create({
  topBar: {
    backgroundColor: 'red',
    height: 25,
  },
  searchBar: {
    backgroundColor: '#6239ab',
    height: 60
  },
  searchInput: {
    margin: 8,
    backgroundColor: 'white',
    borderRadius: 4,
    height: 44,
    paddingLeft: 45,
    paddingRight: 20,
  },
  popularCharactersViewContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#D5DBE0'
  },
  popularCharactersView: {
    margin: 8
  },
  charactersView: {
    padding: 8
  },
  popularCharactersText: {
    color: '#6C7680',
    fontSize: 22
  },
  chaptersView: {
    margin: 8,
    height: 140,
    position: 'relative'
  },
  imageText: {
    color: '#ffffff',
    fontSize: 24,
    position: 'absolute',
    left: 12,
    top: 5,
    fontWeight: 'bold'
  },
  searchCharacter: {
    height: 106,
    flexDirection: 'row',
    padding: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#6C7680'
  }
});


class HomeScreen extends React.Component {

  static navigationOptions = {
    title: 'Welcome',
    header: null
  };

  constructor(){
    super();

    this.state = {
      personSearch: ''
    };

    this.redirectToConstructor = this.redirectToConstructor.bind(this);
    this.redirectToCategory = this.redirectToCategory.bind(this);
    this.renderCharacter = this.renderCharacter.bind(this);
    this.renderCategory = this.renderCategory.bind(this);
    this.searchPerson = this.searchPerson.bind(this);
  }

  searchPerson(text){
    // send request
    if (text.length){
      this.props.apiProvider.searchForCharacter(text);
    }
    this.setState({personSearch: text})
  }

  renderCharacter(character){
    return (
      <TouchableOpacity key={character.id} onPress={()=>this.redirectToConstructor(character)} style={{backgroundColor: '#ffffff', borderRadius: 4, margin: 8}}>
        <View style={styles.popularCharactersView} key={character.id}>
          <CategoryImage source={{uri:`${IMAGE_URL}${character.image}/`}}
                         style={{width: 90, height: 90}}/>
          <Text style={{fontSize: 16, color: '#6C7680', textAlign: 'center'}}>{character.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  redirectToConstructor(meme){
    this.props.navigation.dispatch(NavigationActions.navigate({ routeName: 'Constructor', params: {meme: meme}}));
  }

  redirectToCategory(category){
    this.props.navigation.dispatch(NavigationActions.navigate({ routeName: 'Category', params: {category: category}}));
  }

  renderCategory(category){
    return (
      <TouchableOpacity key={category.id} onPress={()=>this.redirectToCategory(category)}>
        <View style={styles.chaptersView}>
          <CategoryImage source={{uri:`${IMAGE_URL}${category.image}/`}}
                         style={{height: 140, borderRadius: 4}}/>
          <Text style={styles.imageText}>{category.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  renderSearchCharacter(character){
    return (
      <TouchableOpacity key={character.id} onPress={()=>this.redirectToConstructor(character)}>
        <View key={character.id} style={styles.searchCharacter}>
          <CharacterImage source={{uri:`${IMAGE_URL}${character.image}/`}}
                          style={{height: 90, width: 90, borderRadius: 4}}/>
          <View style={{paddingLeft: 14, paddingTop: 22}}>
            <Text style={{fontSize: 24, color: '#6C7680'}}>{character.name}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const {personSearch} = this.state;
    const {popularCharacters, categorises, searchCharacters} = this.props;

    return ([
      <TopBar key={1}/>,
      <View key={2}>
        <View style={styles.searchBar}>
          <TextInput
            style={styles.searchInput}
            onChangeText={this.searchPerson}
            value={personSearch}
            maxLength={15}
            placeholder={'Search for a character'}
            inlineImageLeft={'../static/images/search-icon.png'}
            inlineImagePadding={5}
            underlineColorAndroid='rgba(0,0,0,0)'
            fontSize={18}
          />
          <View style={{position: 'absolute', top: 16, left: 16}}>
            <SearchIcon/>
          </View>
        </View>
        <ScrollView>
          {
            personSearch.length?
              <View style={{marginBottom: 100}}>
                {
                  searchCharacters.map(searchCharacter=>this.renderSearchCharacter(searchCharacter))
                }
              </View>:
              <View style={{marginBottom: 100}}>
                <View style={styles.charactersView}>
                  <Text style={styles.popularCharactersText}>Popular Characters:</Text>
                  <ScrollView horizontal={true} style={styles.popularCharactersViewContainer}>
                    {
                      popularCharacters.map(character=>this.renderCharacter(character))
                    }
                  </ScrollView>
                </View>
                {
                  categorises.results.map(category=>this.renderCategory(category))
                }
              </View>
          }
        </ScrollView>
      </View>
    ]);
  }
}

export default connect(
  state => ({
    popularCharacters: state.api.popularCharacters,
    searchCharacters: state.api.searchCharacters,
    categorises: state.api.categorises,
  }),
  dispatch => ({
    apiProvider: new ApiProvider(dispatch)
  })
)(HomeScreen)
