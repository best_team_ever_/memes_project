import React from 'react';
import {connect} from "react-redux";
import ApiProvider from "../services/api";
import {
  StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, Image, Button,
  TouchableWithoutFeedback, Vibration, Animated
} from 'react-native';
import {PartGroupImage, PartGroupFullImage} from "../utils/image-containers";
import TopBar from "./TopBar";
import {CloseBtn, FontBtn, BackgroundBtn, NextBtn, EyesGroup, FaceGroup, HatGroup, LeftHandGroup, RightHandGroup} from '../constants/images';
import {IMAGE_URL, SCREEN_HEIGHT, SCREEN_WIDTH} from "../settings";
import {NavigationActions} from "react-navigation";
import { ImagePicker, Permissions } from 'expo';


const AVAILABLE_PART_GROUPS = ['face', 'eyes', 'hats', 'right_hand', 'left_hand', 'shirt'];


const styles = StyleSheet.create({
  headerView: {
    height: 60,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#D5DBE0',
    flexDirection: 'row',
  },
  headerIcons: {
    margin: 20,
  },
  lastHeaderIcons: {
    flexDirection: 'row',
    margin: 20,
    alignSelf: 'flex-end',
  },
  continueText: {
    paddingRight: 10,
    color: '#5F27CD',
    fontSize: 16,
    lineHeight: 18
  },
  partGroupsContainer: {
    // width: '100%',
    backgroundColor: '#fff',
    // height: 25,
    // flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: '#D5DBE0',
    justifyContent: 'space-around'
    // shadowColor: '#D5DBE0',
    // shadowOffset: {width: 10, height: 10},
    // shadowRadius: 10,
  },
  partsContainer: {
    backgroundColor: '#f5f5f5',
  },
  currentPartGroup: {
    padding: 5,
    borderBottomWidth: 5,
    // flex: 1,
    margin: 'auto',
    borderColor: '#5F27CD',
    justifyContent: 'space-around'
  },
  partGroup: {
    padding: 5,
    // flex: 1,
    margin: 'auto',
    justifyContent: 'space-around'
  },
  partChosen: {
    borderWidth: 1,
    borderColor: '#aeb2b5',
    borderRadius: 4,
    margin: 9
  },
  part: {
    height: 100,
    borderRadius: 4,
    margin: 10
  },
  imagePickerContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderTopColor: '#aeb2b5'
  },
  buttons: {
    backgroundColor: '#fff',
    height: 50,
    padding: 15,
  },
  backgroundMenuText: {
    fontSize: 20,
    padding: 15,
    fontWeight: 'bold'
  }
});


class Constructor extends React.Component {

  static navigationOptions = {
    title: 'Constructor',
    header: null
  };

  constructor(props){
    super(props);

    this.meme = props.navigation.state.params.meme;

    this.state = {
      loading: true,
      currentPartGroup: '',
      partGroups: [],
      partNameChosen: {
        face: '',
        hats: '',
        eyes: '',
        left_hand: '',
        right_hand: '',
        body: '',
        shirt: ''
      },
      showBackgroundMenu: false,
      background: null,
      pan: new Animated.ValueXY()
    };

    Permissions.askAsync(Permissions.CAMERA).then(()=>Permissions.askAsync(Permissions.CAMERA_ROLL));

    this.props.apiProvider.getMeme(this.meme.id);

    this.renderParts = this.renderParts.bind(this);
    this.renderPartGroupIcon = this.renderPartGroupIcon.bind(this);
    this.renderPartGroups = this.renderPartGroups.bind(this);
    this.choosePart = this.choosePart.bind(this);
    this.renderCurrentImage = this.renderCurrentImage.bind(this);
    this.backgroundSet = this.backgroundSet.bind(this);
    this.chooseBackground = this.chooseBackground.bind(this);
    this.closeBackgroundMenu = this.closeBackgroundMenu.bind(this);

    this._removeBackground = this._removeBackground.bind(this);
    this._selectPicture = this._selectPicture.bind(this);
    this._takePicture = this._takePicture.bind(this);
  }

  componentWillMount() {

    // Add a listener for the delta value change
    this._val = { x:0, y:0 };
    this.state.pan.addListener((value) => this._val = value);

    // Initialize PanResponder with move handling
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gesture) => true,
      onPanResponderMove: Animated.event([null, { dx: this.state.pan.x, dy: this.state.pan.y }]),
      this.state.pan.setValue({ x: 0, y: 0})
    });
  }

  componentWillReceiveProps(nextProps){
    if (this.state.loading&&nextProps.memes[this.meme.id]){

      let partNameChosen = {};

      let face = nextProps.memes[this.meme.id].find(partGroup=>partGroup.name==='face');
      if (face&&face.parts.length){
        partNameChosen.face = face.parts[0].name;
      }

      let hats = nextProps.memes[this.meme.id].find(partGroup=>partGroup.name==='hats');
      if (hats&&hats.parts.length){
        partNameChosen.hats = hats.parts[0].name;
      }

      let eyes = nextProps.memes[this.meme.id].find(partGroup=>partGroup.name==='eyes');
      if (eyes.parts.length){
        partNameChosen.eyes = eyes.parts[0].name;
      }

      let left_hand = nextProps.memes[this.meme.id].find(partGroup=>partGroup.name==='left_hand');
      if (left_hand&&left_hand.parts.length){
        partNameChosen.left_hand = left_hand.parts[0].name;
      }

      let right_hand = nextProps.memes[this.meme.id].find(partGroup=>partGroup.name==='right_hand');
      if (right_hand&&right_hand.parts.length){
        partNameChosen.right_hand = right_hand.parts[0].name;
      }

      let body = nextProps.memes[this.meme.id].find(partGroup=>partGroup.name==='body');
      if (body&&body.parts.length){
        partNameChosen.body = body.parts[0].name;
      }

      let shirt = nextProps.memes[this.meme.id].find(partGroup=>partGroup.name==='shirt');
      if (shirt&&shirt.parts.length){
        partNameChosen.shirt = shirt.parts[0].name;
      }

      this.setState({
        loading: false,
        partGroups: nextProps.memes[this.meme.id].map(partGroup=>partGroup.name),
        currentPartGroup: nextProps.memes[this.meme.id][0]?'face':'', //nextProps.memes[this.meme.id][0].name:'',
        partNameChosen: {
          ...this.state.partNameChosen,
          ...partNameChosen
        }
      })
    }
  }

  renderPartGroupIcon(partGroupName){
    const {currentPartGroup} = this.state;

    let partGroupImage = null;

    if (partGroupName==='face'){
      partGroupImage = FaceGroup({}, currentPartGroup===partGroupName)
    }

    if (partGroupName==='hats'){
      partGroupImage = HatGroup({}, currentPartGroup===partGroupName)
    }

    if (partGroupName==='eyes'){
      partGroupImage = EyesGroup({}, currentPartGroup===partGroupName)
    }

    if (partGroupName==='left_hand'){
      partGroupImage = LeftHandGroup({}, currentPartGroup===partGroupName)
    }

    if (partGroupName==='right_hand'){
      partGroupImage = RightHandGroup({}, currentPartGroup===partGroupName)
    }

    return (
      <TouchableOpacity key={partGroupName} style={currentPartGroup===partGroupName?styles.currentPartGroup:styles.partGroup} onPress={()=>this.setState({currentPartGroup: partGroupName})}>
        {partGroupImage}
      </TouchableOpacity>
    )
  }

  renderPartGroups(){
    const {partGroups, loading} = this.state;

    if (loading){
      return null;
    }

    let partGroupsToRender = [];

    for (let partGroupName of AVAILABLE_PART_GROUPS){
      if (partGroups.indexOf(partGroupName)!==-1){
        partGroupsToRender.push(this.renderPartGroupIcon(partGroupName));
      }
    }

    return partGroupsToRender;
  }

  choosePart(name){
    this.setState({
      partNameChosen: {
        ...this.state.partNameChosen,
        [this.state.currentPartGroup]: name
      }
    })
  }

  chooseBackground(response){
    if (response.didCancel) {
      // console.warn('User cancelled image picker');
    }
    else if (response.error) {
      // console.warn('ImagePicker Error: ', response.error);
    }
    else if (response.customButton === 'removeBackground'){
      this.setState({
        background: null
      });
    }else{
      this.setState({
        background: response.uri
      });
    }
  }

  backgroundSet(){
    this.setState({
      showBackgroundMenu: true
    });
  }

  _selectPicture() {
    ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [1, 1]
    }).then((result)=>{
        if (!result.cancelled){
          this.setState({
            background: result.uri,
            showBackgroundMenu: false
          })
        }
      }
    );
  }

  _takePicture() {
    ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [1, 1]
    }).then((result)=>{
        if (!result.cancelled){
          this.setState({
            background: result.uri,
            showBackgroundMenu: false
          })
        }
      }
    );
  }

  _removeBackground(){
    this.setState({
      background: null,
      showBackgroundMenu: false
    })
  }

  closeBackgroundMenu(){
    this.setState({
      showBackgroundMenu: false
    })
  }

  renderParts(){
    const {partGroups, memes} = this.props;
    const {currentPartGroup, partNameChosen} = this.state;

    let parts = memes[this.meme.id];

    if (!parts){
      return null;
    }

    parts = parts.find(partGroup=>partGroup.name===currentPartGroup);

    if (parts){
      let toRender = [];
      parts = parts.parts;

      for (let part of parts){
        toRender.push(
          <TouchableOpacity style={part.name===partNameChosen[currentPartGroup]?styles.partChosen:styles.part} onPress={()=>this.choosePart(part.name)} key={part.name}>
            <PartGroupImage source={{uri:`${IMAGE_URL}${part.thumbnail}/`}} style={{width: 100, height: 100}}/>
          </TouchableOpacity>
        )
      }
      return toRender;
    }

    return null;
  }

  renderCurrentImage(){
    const {partNameChosen, partGroups, background} = this.state;
    const {memes} = this.props;

    let parts = memes[this.meme.id];

    if (!parts){
      return null;
    }

    let toRender = [];

    if (background !== null){
      toRender.push(
        <Image source={{uri: background}} style={{width: SCREEN_WIDTH, height: SCREEN_WIDTH , position: 'absolute', flex: 1}} key={'background'}/>
      );
    }

    for (let group of partGroups){
      if (partNameChosen[group]!==''&&AVAILABLE_PART_GROUPS.indexOf(group)!==-1){
        let partRender = parts.find(item=>item.name===group).parts.find(item=>item.name===partNameChosen[group]);

        toRender.push(
          <PartGroupFullImage source={{uri:`${IMAGE_URL}${partRender.image}/`}} style={{width: SCREEN_WIDTH, height: SCREEN_WIDTH , position: 'absolute', flex: 1}} key={partRender.name}/>
        )
      }
    }

    return toRender;
  }

  render(){
    const {showBackgroundMenu} = this.state;

    return [
      <TopBar key={1}/>,
      <View style={styles.headerView} key={2}>
        <TouchableOpacity style={styles.headerIcons} onPress={()=>
          this.props.navigation.dispatch(NavigationActions.back())
        }>
          <CloseBtn/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.headerIcons} onPress={()=>{}}>
          <FontBtn/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.headerIcons} onPress={this.backgroundSet}>
          <BackgroundBtn/>
        </TouchableOpacity>
        <View style={{flex: 1}}/>
        <TouchableOpacity style={styles.lastHeaderIcons} onPress={()=>{}}>
          <Text style={styles.continueText}>CONTINUE</Text>
          <NextBtn/>
        </TouchableOpacity>
      </View>,
      <View key={3} style={{flex: 1}}>
        {
          this.renderCurrentImage()
        }
      </View>,
      <View horizontal={true} style={styles.partGroupsContainer} key={4}>
        {
          this.renderPartGroups()
        }
      </View>,
      <View key={5}>
        <ScrollView horizontal={true} style={styles.partsContainer}>
          {
            this.renderParts()
          }
        </ScrollView>
      </View>,
      showBackgroundMenu?
        <TouchableOpacity key={6} style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          opacity: 0.5,
          backgroundColor: '#aeb2b5',
          height: SCREEN_HEIGHT
        }} onPress={this.closeBackgroundMenu} activeOpacity={0.5}>
          <View>
            <Text>321</Text>
          </View>
        </TouchableOpacity>
        :null
      ,
      showBackgroundMenu?
        <TouchableWithoutFeedback key={7}>
          <View style={styles.imagePickerContainer}>
            <Text style={styles.backgroundMenuText}>Background Manage</Text>
            <TouchableOpacity onPress={this._takePicture} style={styles.buttons}>
              <Text>Make a photo</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this._selectPicture} style={styles.buttons}>
              <Text>Choose from gallery</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this._removeBackground} style={styles.buttons}>
              <Text style={{color: '#eb0845'}}>Delete background</Text>
            </TouchableOpacity>
          </View>
        </TouchableWithoutFeedback>
        :null
    ]
  }
}


export default connect(
  state => ({
    memes: state.api.memes,
  }),
  dispatch => ({
    apiProvider: new ApiProvider(dispatch),
  })
)(Constructor)