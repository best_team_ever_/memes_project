import React from 'react';
import {View, StyleSheet} from "react-native";
import {connect} from "react-redux";
import ApiProvider from "../services/api";
import {LoadingImage} from "../constants/images";
import {SCREEN_HEIGHT, SCREEN_WIDTH} from "../settings";
import { NavigationActions } from 'react-navigation';


const styles = StyleSheet.create({
  loadingContainer: {
    backgroundColor: 'black'
  }
});

class LoadingScreen extends React.Component {

  static navigationOptions = {
    title: 'Loading',
    header: null
  };

  constructor(props){
    super(props);

    const partPromise = Promise.all([
      props.apiProvider.getCategories(),
      props.apiProvider.getPopularCharacters(),
    ]).then(
      () => {
        props.apiProvider.doneLoading();
        props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Home' })],
        }));
      }
    );
  }

  render(){
    return (
      <View style={styles.loadingContainer}>
        {
          LoadingImage({width: SCREEN_WIDTH, height: SCREEN_HEIGHT})
        }
      </View>
    );
  }
}

export default connect(
  state => ({
    loading: state.api.loading,
  }),
  dispatch => ({
    apiProvider: new ApiProvider(dispatch)
  })
)(LoadingScreen)