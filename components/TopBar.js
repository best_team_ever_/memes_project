import React from "react";
import { StyleSheet, Text, View, ScrollView, TextInput } from 'react-native';
import PropTypes from 'prop-types';


export default class TopBar extends React.Component {
  static propTypes = {
    color: PropTypes.string
  };

  render() {
    return (
      <View style={{height: 25, backgroundColor: this.props.color?this.props.color:'#502ea4'}}/>
    );
  }
}